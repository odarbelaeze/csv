"""
Usage:
    reader [options] <input> <output>
    reader [options] <input> <output> [<usecols> ...]

Options:
    -c, --comments <comment>        string indicating the comments
    -d, --delimiter <delimiter>     string used to separate values
    -s, --skiprows <number>        skip the first rows in the file
    -h, --help                      shows this help message
    --version                       shows the version of the program
"""

import docopt
import numpy


def clear_file(args, name, mode='rb'):
    """
    Open a file in the given mode according to the data passed
    trough args.

    :param dic args: Arguments
    :param str name: Name of the parameter mapping to the file
    :param str mode: Mode to open the file
    :returns: A filehandler to the specified file
    :rtype: file
    """
    return open(args['<' + name + '>'], mode)


def clear_arguments(args, descriptors, multiple=None):
    """
    Clears arguments from from the passed dict, casts the result using
    the given descriptors.

    :param dict args: Arguments
    :param list descriptors: Descriptors of the given arguments
    :param list multiple: Descriptors for list parameters
    :returns: A dictionary with the parameter names and values
    :rtype: dict
    """
    cleared_arguments = {}

    for name, constructor in descriptors:
        identifier = '--' + name
        if identifier in args and args[identifier] is not None:
            cleared_arguments[name] = constructor(args['--' + name])

    for name, constructor in multiple:
        identifier = '<{0}>'.format(name)
        if identifier in args and len(args[identifier]) > 0:
            cleared_arguments[name] = [
                constructor(x) for x in args[identifier]]

    return cleared_arguments


def handle_data(args):
    """
    Loads data from an input file and write it to another one applying
    the specified modifiers.

    :param dict args: Arguments
    """
    input_file = clear_file(args, 'input', 'rb')
    output_file = clear_file(args, 'output', 'wb')
    load_args = clear_arguments(args, [
        ('comments', str),
        ('delimiter', str),
        ('skiprows', int,), ],
        multiple=[
            ('usecols', int)
        ])
    data = numpy.loadtxt(input_file, **load_args)
    numpy.savetxt(output_file, data)


def main():
    """
    Hnadle console arguments and act upon them
    """
    args = docopt.docopt(__doc__, version='0.0.1', help=True)
    handle_data(args)

if __name__ == '__main__':
    main()
